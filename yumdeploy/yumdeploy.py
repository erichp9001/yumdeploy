'''
Created on Jun 16, 2014

@author: ericr
'''

import os
import os.path
import rpm

def main():
    pass

class rpms(object):
    def __init__(self, base=None):
        
        if base is None:
            try:
                base = os.environ['IWS_TESTBASE']
            except KeyError:
                base = None
        logname = os.environ['LOGNAME']
        if base is None:
            if logname is not None:
                base = "/work/"+logname+"/iws_tests/test_lib/SRPMS"
            else:
                raise "IWS_TESTBASE and LOGNAME are not set"         
        self._iws_testbase = base
          
    def getRpmList(self):
        l = [ f for f in os.listdir(self._iws_testbase) if os.path.isfile(os.path.join(self._iws_testbase,f)) ]
        return l
    
    def installSource(self,filename):
        os.system("rpm -i "+self._iws_testbase+'/'+filename)
        
    def buildRpm(self):
        ''' Build the rpm in the rpmbuild structure '''
        # TBD:  get back results of command for error reporting
        base = os.environ['HOME']+'/rpmbuild/SPECS'
        l = [ f for f in os.listdir(base) if os.path.isfile(os.path.join(base,f)) ]
        for r in l:
            os.system("rpmbuild -bb "+os.path.join(base,r))
            
        pass
        
    
if __name__ == '__main__':
    pass