'''
Created on Jun 16, 2014

@author: ericr
'''

import os
import unittest
import shutil

from yumdeploy import yumdeploy


class Test(unittest.TestCase):

    def _getRpmBase(self,baseidx=0):
        bases = ['/rpms','/SRPMS']
        return os.path.dirname(os.path.realpath(__file__)) + bases[baseidx]

    def setUp(self):
        os.environ['IWS_TESTBASE'] = self._getRpmBase(0)


    def tearDown(self):
        del os.environ['IWS_TESTBASE']


    def testMain(self):
        yumdeploy.main()
     
    def testRpm(self):
        r = yumdeploy.rpms()
        self.assertEqual(r._iws_testbase, self._getRpmBase(0))  
        del r
        r = yumdeploy.rpms()
        self.assertEqual(r._iws_testbase, self._getRpmBase(0)) 
        
    def testGetRpmList(self):
        r = yumdeploy.rpms()
        l = r.getRpmList()
        self.assertNotEqual(l,None)
        goodlist = ['a.rpm','b.rpm','c.rpm','e.rpm']
        self.assertEquals(len(l), len(goodlist))
        
    def testUnpackSrpm(self):
        os.environ['IWS_TESTBASE'] = self._getRpmBase(1)
        rpmbuildroot = os.environ['HOME']+'/rpmbuild'
        if os.path.exists(rpmbuildroot):
            self.assert_(False, "~/rpmbuild already exists.  Remove that directory before running test.")
        try:
            r = yumdeploy.rpms()
            l = r.getRpmList()
            rpmfile =  'simple-1.0-1.el6.src.rpm'
            self.assertEquals(l[0],rpmfile)
            r.installSource(l[0])
            self.assertTrue(os.path.exists(rpmbuildroot+'/SPECS/simple.spec'), "spec file simple.spec is missing")
        finally:
            if os.path.exists(rpmbuildroot):
                shutil.rmtree(rpmbuildroot)
        
    def testBuildRpm(self):
   
        os.environ['IWS_TESTBASE'] = self._getRpmBase(1)
        rpmbuildroot = os.environ['HOME']+'/rpmbuild'
        if os.path.exists(rpmbuildroot):
            self.assert_(False, "~/rpmbuild already exists.  Remove that directory before running test.")
        try:
            r = yumdeploy.rpms()
            l = r.getRpmList()
            r.installSource(l[0])
            r.buildRpm()
            self.assertTrue(os.path.exists(rpmbuildroot+'/RPMS/noarch/simple-1.0-1.el6.noarch.rpm'))
        finally:
            if os.path.exists(rpmbuildroot):
                shutil.rmtree(rpmbuildroot)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()